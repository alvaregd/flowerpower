package com.gorillamoa.flower.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.gorillamoa.flower.FlowerPower;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();

		/** set the screen size */
		config.width = FlowerPower.WIDTH;
		config.height = FlowerPower.HEIGHT;
		config.title = FlowerPower.TITLE;

		new LwjglApplication(new FlowerPower(), config);
	}
}
