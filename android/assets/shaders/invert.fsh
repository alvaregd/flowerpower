varying vec4 v_color;
varying vec2 v_texCoord0;

//set the color of the pixel

//sample single pixels from texture. use it to get the pixels
uniform sampler2D u_sampler2D;

void main(){
    //we went to invert the colors of our texture, so
    // try 1 - rgb

    vec4 color = texture2D(u_sampler2D, v_texCoord0) * v_color ;

    color.rgb = 1. - color.rgb;
    gl_FragColor =  color;


}
