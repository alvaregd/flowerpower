varying vec4 v_color;
varying vec2 v_texCoord0;

uniform vec2 u_resolution;
uniform vec2 u_centerPosition;
uniform sampler2D u_sampler2D;

const float outerRadius =0.6, innerRadius =.2, intensity = .6;

void main(){
    vec4 color = texture2D(u_sampler2D, v_texCoord0) * v_color ;

//  vec4 color = vec4(1.0,0.0,0.0,1.0);
    //position relative to center
    //gl_FragCoord tells us coordinate of different pixels
    //soo the coordinate devided by the resolution, offset by half the screen in both xy axis
    vec2 relativePosition = gl_FragCoord.xy / u_resolution - u_centerPosition / u_resolution;
//    vec2 relativePosition = gl_FragCoord.xy / u_resolution -.5;

    //now abs the position
    float len = length(relativePosition); //length returns the vector length
    float vignette = smoothstep(outerRadius, innerRadius,len);

    color.rgb = mix(color.rgb, color.rgb * vignette, intensity);  //mix two color stuff

    gl_FragColor =  color;

}
