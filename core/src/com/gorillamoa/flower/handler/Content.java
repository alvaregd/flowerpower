package com.gorillamoa.flower.handler;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import java.util.HashMap;

/** Load up files needed by bondfire and games */
public class Content {

    private final static String Tag = Content.class.getName();
    private final static boolean d_LoadProfilePictures = false;
    private final static boolean d_getSmallIcon =false;
    private final static boolean d_getLargeIcon =false;

    /** atlases */
    HashMap<String, TextureAtlas> atlases;

    /** shaders */
    HashMap<String, FileHandle> fragmentShaders;
    HashMap<String, FileHandle> vertexShaders;

    /** font generators */
    BitmapFont bmFont;
    String fontPath;

    /** Profile Images **/
    HashMap<String, TextureRegion> smallIcons;
    HashMap<String, TextureRegion> largeIcons;
    HashMap<String, String> smallIconUrls;
    HashMap<String, String> largeIconUrls;

    public Content(){
        atlases = new HashMap<String, TextureAtlas>();
        vertexShaders = new HashMap<String, FileHandle>();
        fragmentShaders = new HashMap<String, FileHandle>();
        smallIcons = new HashMap<String, TextureRegion>();
        largeIcons = new HashMap<String, TextureRegion>();
        smallIconUrls = new HashMap<String, String>();
        largeIconUrls = new HashMap<String, String>();
    }

    /** here we will fetch the assets and put them int memory*/
    public void LoadAtlas(String path, String key){
        atlases.put(key, new TextureAtlas(Gdx.files.internal(path)));
    }

    public TextureAtlas getAtlas(String key ){
        return atlases.get(key);
    }

    public void LoadShaders(String vshPath, String fshPath, String key){
        vertexShaders.put(key, Gdx.files.internal(vshPath));
        fragmentShaders.put(key, Gdx.files.internal(fshPath));
    }

    public FileHandle[] getShaders(String key){
        FileHandle[] handles = new FileHandle[2];
        handles[0] =vertexShaders.get(key);
        handles[1] =fragmentShaders.get(key);
        return handles;
    }

    public void LoadFont(String path){
        this.fontPath = path;
    }

    public void GeneratorFont(){

    }

    public void dispose(){
        if(bmFont != null) bmFont = null;
    }

    public BitmapFont getBmpFont(){
        if(bmFont == null){
            GeneratorFont();
        }
        return bmFont;
    }

    //loads both large and small icons
    public void LoadProfilePictures(String[] small, String[] large){
        smallIconUrls.clear();
        largeIconUrls.clear();

        for(int i =0; i < small.length; i++){
            smallIconUrls.put("p_" + (i+1), small[i]);
            largeIconUrls.put("p_" + (i+1), large[i]);
        }
    }

    public String getLargeIconUrl(int i ){
        return largeIconUrls.get("p_" + i);
    }

    public String getSmallIconUrl(int i ){
        return smallIconUrls.get("p_" + i);
    }

}

