package com.gorillamoa.flower;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.gorillamoa.flower.handler.Content;
import com.gorillamoa.flower.states.CanvasState;
import com.gorillamoa.flower.states.GSM;

public class FlowerPower extends ApplicationAdapter {

	public static final String SHADER_KEY_GRADIENT = "gradientColor";
	public static final String TITLE= "Flower Power";
	public static final int WIDTH  = 900;
	public static final int HEIGHT = 900;

	private GSM gsm;

	/** our assets */
	public static Content res;

	/** we use it to draw */
	private SpriteBatch sb;

	@Override
	public void create () {
		Gdx.app.setLogLevel(Application.LOG_INFO);
		Gdx.gl.glClearColor(0.2f,0.2f,0.2f,1);

		res = new Content();

		/** load our shader descriptor files */
		res.LoadShaders("gradientColor.vertexsh","gradientColor.fsh",SHADER_KEY_GRADIENT);
		res.LoadAtlas("flower.pack","flower");

		sb = new SpriteBatch();
		gsm = new GSM();

		gsm.push(new CanvasState(this.gsm));
	}

	@Override
	public void render () {

		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		gsm.update(Gdx.graphics.getDeltaTime());
		gsm.render(sb);
	}
}
