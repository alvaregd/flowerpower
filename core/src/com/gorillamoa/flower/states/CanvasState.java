package com.gorillamoa.flower.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.gorillamoa.flower.FlowerPower;
import com.gorillamoa.flower.sprites.Flower;

public class CanvasState extends State {

    private Flower flower;
//    private Flower pot;
    ShaderProgram colorShader;

    public CanvasState(GSM gsm) {
        super(gsm);

        ShaderProgram.pedantic = false;
        colorShader = new ShaderProgram(FlowerPower.res.getShaders(FlowerPower.SHADER_KEY_GRADIENT)[0],
                FlowerPower.res.getShaders(FlowerPower.SHADER_KEY_GRADIENT)[1]);
        System.out.println(colorShader.isCompiled() ? "Shader 1 compiled, yay" : colorShader.getLog());
        Gdx.app.log("Shader:",colorShader.getLog());

        /** create our objects **/
        flower = new Flower(FlowerPower.WIDTH/2, FlowerPower.HEIGHT/2);
        flower.isGrowing(true);
//        flower.setSpinRate(3);
        flower.setSpinRate(5);
        flower.setLeafsPerRevolution(8);
//        flower.setLeafsPerRevolution(50);

       /* pot = new Flower(FlowerPower.WIDTH/2, FlowerPower.HEIGHT*1/4);
        pot.isGrowing(true);
        pot.setSpinRate(2);
        pot.setLeafsPerRevolution(5);*/
    }

    @Override
    public void update(float dt) {
        flower.update(dt);
//        pot.update(dt);
    }

    @Override
    public void render(SpriteBatch sb) {

        sb.setProjectionMatrix(cam.combined);
        sb.setShader(colorShader);
        flower.render(sb, colorShader);
//        pot.render(sb,colorShader);
    }

    @Override
    public void handleInput() {

    }
}
